module gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-process

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gofiber/fiber/v2 v2.7.1
	github.com/klauspost/compress v1.11.13 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/svent/go-flags v0.0.0-20141123140740-4bcbad344f03 // indirect
	github.com/valyala/fasthttp v1.23.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210326220804-49726bf1d181 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
