// Licensed under the MIT license
package app

type RoadInterruption struct {
	Road string `json:"road"`
}
