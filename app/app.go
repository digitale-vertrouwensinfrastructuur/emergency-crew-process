// Licensed under the MIT license
package app

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

func App(roadInterruptionServiceURL string) *fiber.App {
	app := fiber.New()

	app.Use(requestid.New())

	app.Use(logger.New(logger.Config{
		Format: "${pid} ${locals:requestid} ${status} - ${method} ${path}\n",
	}))

	app.Use(cors.New(cors.Config{
		AllowOrigins: "https://routeplanner.fire-brigade.dvi.app",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	app.Get("/health", func(c *fiber.Ctx) error {
		return c.SendString("healthy")
	})

	app.Get("/api/v1/calculate-route", func(c *fiber.Ctx) error {
		response, err := http.Get(roadInterruptionServiceURL)
		if err != nil {
			c.Status(500)
			return c.SendString("Unable to contact " + roadInterruptionServiceURL)
		}
		defer response.Body.Close()

		bodyBytes, _ := ioutil.ReadAll(response.Body)

		if response.StatusCode != 200 {
			c.Status(500)
			return c.SendString("Unable to fetch RoadInterruptions: " + response.Status + "\nBody: " + string(bodyBytes))
		}

		var roadInterruptions []RoadInterruption
		err = json.Unmarshal(bodyBytes, &roadInterruptions)
		if err != nil {
			c.Status(500)
			return c.SendString("Unable to unmarshall RoadInterruptions: " + err.Error())
		}
		return c.JSON(roadInterruptions)
	})

	return app
}
