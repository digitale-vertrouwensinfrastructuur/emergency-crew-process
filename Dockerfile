# Build
FROM golang:1.16-alpine AS build

WORKDIR /go/src/app
ADD . /go/src/app/

RUN go mod download
RUN go build -o ./dist/bin/emergency-crew-process ./cmd/emergency-crew-process/main.go

# Release 
FROM alpine:latest

COPY --from=build /go/src/app/dist/bin/emergency-crew-process /usr/local/bin/emergency-crew-process

RUN adduser -D -u 1001 appuser
USER appuser

EXPOSE 8081

CMD ["/usr/local/bin/emergency-crew-process"]
