# Emergency crew process
The emergency crew process is a process that uses data over NLX.

## Requirements
- go 1.16+

## Building
### Build using go
```
go build ./...
```

## Running locally
### Run using go
```
go run ./cmd/emergency-crew-process/main.go
```
### Run using Skaffold
Install microk8s according to [Microk8s installation](https://microk8s.io/docs/install-alternatives)
Install Helm according to [Helm installation](https://helm.sh/docs/intro/install/)
Install Skaffold according to [Skaffold installation](https://skaffold.dev/docs/install/)
Copy the values from `microk8s.kubectl config view` to $HOME/.kube/config

```bash
docker login registry.gitlab.com # a gitlab access token can be used as password here

kubectl config use-context microk8s
kubectl create ns dvi-fire-brigade
kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=</absolute/path/to/.docker/config.json> \
    --type=kubernetes.io/dockerconfigjson \
    --namespace=dvi-fire-brigade
```
This `regcred` secret will be used by the Helm deployment to fetch the Docker image. Make sure to create it in the correct namespace.

Alternatively, it is also possible to create the regcred using:
```
kubectl create secret docker-registry regcred --docker-server=registry.gitlab.com --docker-username=<username> --docker-password=<gitlab-access-token> --docker-email=<email> --namespace=dvi-fire-brigade
```

Then run:

```bash
skaffold dev --cleanup=false -p fire-brigade
```

It may take some time for all resources to become healthy.

To test that the emergency-crew-process is up, make sure you can connect to it, for example using a port-forward

```bash
kubectl port-forward service/emergency-crew-service 8080:http --namespace=dvi-fire-brigade
curl localhost:8080/health
```
### Run using Docker-compose
```
docker-compose up
```
## Testing

### Testing REST endpoints manually
The `examples/emergency-crew-process.http` file can be used to test the REST endpoint

## License
See [LICENSE.md](LICENSE.md)
