package main

// Licensed under the MIT license
import (
	"log"

	"github.com/svent/go-flags"
	"gitlab.com/digitale-vertrouwensinfrastructuur/emergency-crew-process/app"
)

type Options struct {
	RoadInterruptionServiceUrl string `long:"road-interruption-service-url" env:"ROAD_INTERRUPTION_SERVICE_URL" default:"http://localhost:8080/api/v1/road-interruptions"`
}

var options Options

func main() {
	// Parse arguments
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return // stop running after help flag
			}
		}
		log.Fatalf("error parsing flags: %v", err)
	}
	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}
	app := app.App(options.RoadInterruptionServiceUrl)
	log.Fatal(app.Listen(":8081"))
}
